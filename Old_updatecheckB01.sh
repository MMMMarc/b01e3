#!/bin/sh


# Update parameters. Feel free to change this!
FILENAME=B01I00F00
DEVICESW=M1m26




##########################################################
# WARNING: BE AWARE of changing everythin after this line!
# Upgrading script
# Check if there is a ID.txt file
cd /home/piavita/application

UPDATESERVER=$(sed -n 's|.*"updateserver":"\([^"]*\)".*|\1|p' HWsettings.json)
VERSION=$(sed -n 's|.*"BasestationSW":"\([^"]*\)".*|\1|p' SWsettings.json)
# check if software upgrade exists and if the old is aready finished
if [ $FILENAME = $VERSION ];then
  echo no update
  exit 0
fi
cd /home/piavita/application
#sudo node /home/piavita/application/LEDs_orange.js

#cd /home/piavita/application
#[ ! -d tmp ] && mkdir tmp && chown piavita:piavita tmp
#cd /home/piavita/application/tmp

#cd /home/piavita/application/tmp
#remove fertig file becaus the update is running to make sure it will run again if it was intrerupted
#[ -f fertig.txt ] && rm fertig.txt
# Pre-requirements

#download files eventeuell nur files oder ganzen image
sudo mount -t ubifs ubi0:data /mnt/dataM
sleep 1
#change to other ubi
cd /mnt/dataM

[ -f uImage ] && rm uImage
[ -f efusa7ul.dtb ] && rm efusa7ul.dtb
[ -f rootfs.ubifs ] && rm rootfs.ubifs
[ -f update.scr ] && rm update.scr
#update in mount laden
#kernel
wget https://bitbucket.org/MMMMarc/B01E3/raw/$UPDATESERVER/releases/$FILENAME/uImage
#device tree
wget https://bitbucket.org/MMMMarc/B01E3/raw/$UPDATESERVER/releases/$FILENAME/efusa7ul.dtb
#rootfilesystem
wget https://bitbucket.org/MMMMarc/B01E3/raw/$UPDATESERVER/releases/$FILENAME/rootfs.ubifs
#update script
wget https://bitbucket.org/MMMMarc/B01E3/raw/$UPDATESERVER/releases/$FILENAME/update.scr
cd /mnt/dataM/
sudo nandwrite -p /dev/mtd7 update.scr
sleep 1
sudo umount -t ubifs ubi0:data


cd /home/piavita/application
#sudo node LEDs_cyan.js
#cd /home/piavita/application/tmp
#sudo touch fertig.txt
sudo reboot
